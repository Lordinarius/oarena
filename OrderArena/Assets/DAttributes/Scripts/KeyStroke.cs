using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KeyStroke {

    private static Dictionary<int, KeyCom> keyCodes = new Dictionary<int, KeyCom>();

    public enum KeyModifier {
        None,
        Shift,
        Control,
        Alt
    }

    public static void UpdateKeyInputs() {
        foreach (var item in keyCodes) {
            var k = item.Value;

            //bool m1 = Input.GetKey(k.keyMod1);
            //bool m2 = Input.GetKey(k.keyMod2);

            //if (k.keyMod1 == KeyCode.None && k.keyMod2 == KeyCode.None) {
            //    if (Input.GetKeyDown(k.key) && !m1 && !m2) {
            //        k.keyEvent.Invoke();
            //    }
            //} else if (k.keyMod1 != KeyCode.None && k.keyMod2 == KeyCode.None) {
            //    if (Input.GetKeyDown(k.key) && m1) {
            //        k.keyEvent.Invoke();
            //    }
            //} else if (k.keyMod1 != KeyCode.None && k.keyMod2 != KeyCode.None) {
            //    if (Input.GetKeyDown(k.key) && m1 && m2) {
            //        k.keyEvent.Invoke();
            //    }
            //}

            if (GetShift() && GetControl()) {

            } else if (GetShift() && GetAlt()) {

            } else if (GetControl() && GetAlt()) {

            } else {

            }
        }
    }

    private static bool GetShift() {
        return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
    }

    private static bool GetControl() {
        return Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
    }

    private static bool GetAlt() {
        return Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.AltGr);
    }

    //private static KeyCode GetKeyCode(KeyModifier keyModifier) {
    //    switch (keyModifier) {
    //        case KeyModifier.None:
    //            return KeyCode.None;
    //        case KeyModifier.LeftShift:
    //            return KeyCode.LeftShift;
    //        case KeyModifier.LeftControl:
    //            return KeyCode.LeftControl;
    //        case KeyModifier.LeftAlt:
    //            return KeyCode.LeftAlt;
    //        default:
    //            throw new System.Exception(keyModifier.ToString());
    //    }
    //}

    public static KeyCom KeyCommand(KeyCode key, KeyModifier keyMod1 = KeyModifier.None, KeyModifier keyMod2 = KeyModifier.None) {
        int hash = GetHash(key, keyMod1, keyMod2);
        Debug.Log(hash);
        KeyCom keyCom;
        if (keyCodes.TryGetValue(hash, out keyCom)) {
            return keyCom;
        } else {
            keyCom = new KeyCom(key, keyMod1, keyMod2);
            keyCodes.Add(hash, keyCom);
            return keyCom;
        }
    }

    private static int GetHash(KeyCode key, KeyModifier keyMod1 = KeyModifier.None, KeyModifier keyMod2 = KeyModifier.None) {
        int a = (int)key;
        int b = (int)keyMod1;
        int c = (int)keyMod2;
        return (a * 24) * 6 + c * b;
    }

    public class KeyCom {
        public KeyCode key;
        public KeyModifier keyMod1;
        public KeyModifier keyMod2;

        public KeyCom(KeyCode key, KeyModifier keyMod1, KeyModifier keyMod2) {
            this.key = key;
            this.keyMod1 = keyMod1;
            this.keyMod2 = keyMod2;
            keyEvent = null;
        }

        public System.Action keyEvent;

        //public override int GetHashCode() {
        //    int a = (int)key;
        //    int b = (int)keyMod1;
        //    int c = (int)keyMod2;
        //    return (a * 24 + b) * 6 + c;
        //}
    }
}
