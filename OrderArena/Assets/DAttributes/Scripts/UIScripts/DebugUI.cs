using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System;
public class DebugUI : MonoBehaviour {

    private static DebugUI instance;

    public UIDebugButton uiDebugButtonprefab;
    public UIDebugSlider uiDebugSliderPrefab;
    public UIDebugToggle uiDebugTogglePrefab;

    private List<UIDebugSlider> slidersToUpdate = new List<UIDebugSlider>();
    private List<UIDebugToggle> togglesToUpdate = new List<UIDebugToggle>();

    protected List<DebugAttribute> atributeList = new List<DebugAttribute>();

    private RectTransform panelRect;
    private Image panelImage;

    public static void UpdateWidgets() {
        instance.UpdateWidets();
    }

    protected void InitInternal() {
        //var go = new GameObject("DebugPanel");
        //go.transform.SetParent(this.transform);
        panelImage = gameObject.AddComponent<Image>();
        panelRect = gameObject.GetComponent<RectTransform>();
        panelRect.anchorMin = new Vector2(0, 0);
        panelRect.anchorMax = new Vector2(1, 1);
        panelRect.offsetMin = new Vector2();
        panelRect.offsetMax = new Vector2();
        panelImage.color = new Color(0, 0, 0, .6f);
    }

    public void UpdateWidets() {
        foreach (var item in slidersToUpdate) {
            item.slider.normalizedValue = item.getter.Invoke();
        }
        foreach (var item in togglesToUpdate) {
            item.toggle.isOn = item.getter.Invoke();
        }
    }

    public static void Init(DebugUI inst) {
        //var go = new GameObject("DebugPanel");
        instance = inst;
        instance.InitInternal();
    }

    public static void RegisterForDebug(object obj){
        var ats = DAttributeHelper.GetMethodAttributes(obj);
        instance.atributeList.AddRange(ats);
    }

    public static void CreateUI() {
        //var go = new GameObject("DebugPanel");
        instance.CreateUIInternal();
    }

    public static void Clear() {
        instance.atributeList.Clear();
    }

    public void CreateUIInternal() {

        float xspace = 180;
        float yspace = 80;
        int collumnLock = 3;
        float xstart = -xspace * (collumnLock-1) / 2;
        float ystart = -60;

        for (int i = 0; i < atributeList.Count; i++)
        {
            DebugWidget newWidget = null;
            DebugAttribute a = atributeList[i];
            if (a is DebugButtonAttribute) {
                newWidget = MatchButtonAttribute(a as DebugButtonAttribute);
            }
            else if(a is DebugSliderAttribute) {
                newWidget = MatchSliderAttribute(a as DebugSliderAttribute);
            } 
            else if (a is DebugToggleAttribute) {
                newWidget = MatchToggleAttribute(a as DebugToggleAttribute);
            } else {
                throw new Exception(a.ToString());
            }
            int row = i / collumnLock;
            int collumn = i % collumnLock;
            newWidget.rectTransform.SetAnchor(DUtility.Anchor.TOP);
            newWidget.rectTransform.anchoredPosition = new Vector2(xstart + collumn * xspace, ystart - row * yspace);
        }
    }

    private UIDebugButton MatchButtonAttribute(DebugButtonAttribute attribute) {
        var b = Instantiate(uiDebugButtonprefab, transform);
        b.transform.localPosition = new Vector3();
        b.button.onClick.AddListener(attribute.GetDelegate());
        b.label.text = attribute.GetMethodName();
        return b;
    }

    private UIDebugSlider MatchSliderAttribute(DebugSliderAttribute attribute) {
        var b = Instantiate(uiDebugSliderPrefab, transform);
        b.transform.localPosition = new Vector3();
        b.slider.normalizedValue = attribute.initialValue;
        b.slider.onValueChanged.AddListener(attribute.GetDelegate());
        b.label.text = attribute.GetMethodName();
        if (!string.IsNullOrEmpty(attribute.trackingField)) {
            b.getter = attribute.GetterFunc();
            attribute.SetterFunc().Invoke(attribute.initialValue);
            slidersToUpdate.Add(b);
        }
        return b;
    }

    private UIDebugToggle MatchToggleAttribute(DebugToggleAttribute attribute) {
        var b = Instantiate(uiDebugTogglePrefab, transform);
        b.transform.localPosition = new Vector3();
        b.toggle.isOn = attribute.initialValue;
        b.toggle.onValueChanged.AddListener(attribute.GetDelegate());
        b.label.text = attribute.GetMethodName();
        if (!string.IsNullOrEmpty(attribute.trackingField)) {
            b.getter = attribute.GetterFunc();
            attribute.SetterFunc().Invoke(attribute.initialValue);
            togglesToUpdate.Add(b);
        }
        return b;
    }

}
