using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDebugToggle : DebugWidget {
    public Toggle toggle;
    public System.Func<bool> getter;
}
