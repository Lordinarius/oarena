using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebugWidget : MonoBehaviour {

    private RectTransform m_rectTransform;
    public RectTransform rectTransform {
        get { return m_rectTransform ?? (m_rectTransform = GetComponent<RectTransform>()); }
    }

    public Text label;
}
