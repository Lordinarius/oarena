using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDebugSlider : DebugWidget {
    public Slider slider;
    public System.Func<float> getter;
}
