﻿using UnityEngine;

public static class DUtility
{
    public enum Anchor
    {
        CENTER,
        TOP,
        RIGHT,
        LEFT,
        BOTTOM,
        TOPRIGHT,
        TOPLEFT,
        BOTTOMRIGHT,
        BOTTOMLEFT,
    }

    public static void SetAnchor(this RectTransform rectTransform,Anchor anchor,bool self = false)
    {
        switch (anchor)
        {
            case Anchor.CENTER:
                rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
                rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
                if (self) { rectTransform.pivot = new Vector2(0.5f, 0.5f); }
                break;
            case Anchor.TOP:
                rectTransform.anchorMax = new Vector2(0.5f, 1f);
                rectTransform.anchorMin = new Vector2(0.5f, 1f);
                if (self) { rectTransform.pivot = new Vector2(0.5f,1f); }
                break;
            case Anchor.RIGHT:
                rectTransform.anchorMax = new Vector2(1f, 0.5f);
                rectTransform.anchorMin = new Vector2(1f, 0.5f);
                if (self) { rectTransform.pivot = new Vector2(1f, 0.5f); }
                break;
            case Anchor.LEFT:
                rectTransform.anchorMax = new Vector2(0, 0.5f);
                rectTransform.anchorMin = new Vector2(0, 0.5f);
                if (self) { rectTransform.pivot = new Vector2(0, 0.5f); }
                break;
            case Anchor.BOTTOM:
                rectTransform.anchorMax = new Vector2(0.5f, 0);
                rectTransform.anchorMin = new Vector2(0.5f, 0);
                if (self) { rectTransform.pivot = new Vector2(0.5f, 0); }
                break;
            case Anchor.TOPRIGHT:
                rectTransform.anchorMax = new Vector2(1, 1);
                rectTransform.anchorMin = new Vector2(1, 1);
                if (self) { rectTransform.pivot = new Vector2(1, 1); }
                break;
            case Anchor.TOPLEFT:
                rectTransform.anchorMax = new Vector2(0, 1);
                rectTransform.anchorMin = new Vector2(0, 1);
                if (self) { rectTransform.pivot = new Vector2(0, 1); }
                break;
            case Anchor.BOTTOMRIGHT:
                rectTransform.anchorMax = new Vector2(1, 0);
                rectTransform.anchorMin = new Vector2(1, 0);
                if (self) { rectTransform.pivot = new Vector2(1, 0); }
                break;
            case Anchor.BOTTOMLEFT:
                rectTransform.anchorMax = new Vector2(0, 0);
                rectTransform.anchorMin = new Vector2(0, 0);
                if (self) { rectTransform.pivot = new Vector2(0, 0); }
                break;
            
            default: throw new System.Exception(anchor.ToString());
        }
    }
}
