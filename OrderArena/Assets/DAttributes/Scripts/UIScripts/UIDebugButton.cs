using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDebugButton : DebugWidget {
    public Image image;
    public Button button;
}
