using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public abstract class DebugAttribute : System.Attribute {

    public string name;
    public MethodInfo methodInfo;
    public object targetObject;

    public DebugAttribute(string name = "") {
        this.name = name;
    }

    public string GetMethodName() {
        if (string.IsNullOrEmpty(name)) {
            return methodInfo.Name;
        } else {
            return name;
        }
    }
}

[System.AttributeUsage(System.AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
public sealed class DebugButtonAttribute : DebugAttribute {

    public DebugButtonAttribute(string name = "") : base(name) {
        this.name = name;
    }


    public UnityEngine.Events.UnityAction GetDelegate() {
        return delegate { methodInfo.Invoke(targetObject, null); };
    }


}

public class ValueTracked : DebugAttribute {

    public readonly string trackingField;
    public MemberInfo varInfo;

    public ValueTracked(string trackingField) {
        this.trackingField = trackingField;
    }

    public FieldInfo GetFieldInfo() {
        return targetObject.GetType().GetField(trackingField);
    }

    public PropertyInfo GetPropInfo() {
        return targetObject.GetType().GetProperty(trackingField);
    }

    public MemberInfo GetMemberInfo() {
        MemberInfo mi = GetFieldInfo();
        if (mi == null) {
            mi = GetPropInfo();
        }
        if (mi == null) {
            throw new System.Exception("Can not find that field or property (Probably you've entered wrong name)");
        } else {
            return mi;
        }
    }

    protected UnityEngine.Events.UnityAction<T> GetDelegate<T>() {
        return (f) => { methodInfo.Invoke(targetObject, new object[] { f }); };
    }

    protected System.Func<T> GetterFunc<T>() {
        varInfo = GetMemberInfo();
        System.Func<T> getter = null;
        if (varInfo is FieldInfo) {
            FieldInfo fi = varInfo as FieldInfo;
            getter = () => { return (T)fi.GetValue(targetObject); };
        } else if (varInfo is PropertyInfo) {
            PropertyInfo pi = varInfo as PropertyInfo;
            getter = () => { return (T)pi.GetValue(targetObject, null); };
        } else {
            throw new System.Exception(varInfo.ToString());
        }
        return getter;
    }

    protected System.Action<T> SetterFunc<T>() {
        varInfo = GetMemberInfo();
        System.Action<T> setter = null;
        if (varInfo is FieldInfo) {
            FieldInfo fi = varInfo as FieldInfo;
            setter = (val) => fi.SetValue(targetObject, val);
        } else if (varInfo is PropertyInfo) {
            PropertyInfo pi = varInfo as PropertyInfo;
            setter = (val) => pi.SetValue(targetObject, val, null);
        } else {
            throw new System.Exception(varInfo.ToString());
        }
        return setter;
    }
}

[System.AttributeUsage(System.AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
public sealed class DebugSliderAttribute : ValueTracked {

    public readonly float initialValue;

    public DebugSliderAttribute(string name = "", float initialValue = 0,string trackField = "") : base(trackField) {
        this.initialValue = initialValue;
        this.name = name;
    }

    public UnityEngine.Events.UnityAction<float> GetDelegate() {
        return GetDelegate<float>();
    }

    public System.Func<float> GetterFunc() {
        return GetterFunc<float>();
    }

    public System.Action<float> SetterFunc() {
        return SetterFunc<float>();
    }

}

[System.AttributeUsage(System.AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
public sealed class DebugToggleAttribute : ValueTracked {

    public readonly bool initialValue;

    public DebugToggleAttribute(string name = "", bool initialValue = false, string trackField = "") : base(trackField) {
        this.initialValue = initialValue;
        this.name = name;
    }

    public UnityEngine.Events.UnityAction<bool> GetDelegate() {
        return GetDelegate<bool>();
    }

    public System.Func<bool> GetterFunc() {
        return GetterFunc<bool>();
    }

    public System.Action<bool> SetterFunc() {
        return SetterFunc<bool>();
    }

}