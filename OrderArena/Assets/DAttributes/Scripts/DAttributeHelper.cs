using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

public static class DAttributeHelper {

    public static List<DebugAttribute> GetMethodAttributes(object obj)
	{
		List<DebugAttribute> ts = new List<DebugAttribute>(20);
        FillMethodAttributes(ts, obj);
		return ts;
	}

    private static void FillMethodAttributes<T>(List<T> ts, object item) where T : DebugAttribute {
        var type = item.GetType();
        var methods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
        foreach (var m in methods) {
            var ats = (T)Attribute.GetCustomAttribute(m, typeof(T));
            if (ats != null) {
                ats.methodInfo = m;
                ats.targetObject = item;
                ts.Add(ats);
            }
        }
    }
}
