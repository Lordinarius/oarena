using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMethod : MonoBehaviour {

    public float sliderValue;
    public bool toggleValue;
    public DebugUI dUI;

    private void Start() {
        //Debug.Log(DBAttributeHandler.GetAttributesOfAssembly(typeof(TestMethod)).Length);
        DebugUI.Init(dUI);
        DebugUI.RegisterForDebug(this);
        DebugUI.CreateUI();
    }

    public void Update() {
        DebugUI.UpdateWidgets();
    }

    [DebugButton]
    public void TestBut() {
        Debug.Log("AAAAAAAA");
    }
    
    [DebugSlider("Slider Test",0.5f,"sliderValue")]
    public void SiderTest(float val) {
        sliderValue = val;
    }

    [DebugToggle("Toggle", false, "toggleValue")]
    public void ToggeTest(bool val) {
        toggleValue = val;
    }

    [DebugToggle("ToggleA")]
    public void ToggeTestA(bool val)
    {
        toggleValue = val;
    }

    [DebugButton]
    public void TestButB()
    {
        Debug.Log("BBBBB");
    }

    [DebugButton]
    public void TestButC()
    {
        Debug.Log("CCCCCC");
    }

    [DebugSlider("Slider TestAD", 0.5f)]
    public void SiderTestAD(float val)
    {
        sliderValue = val;
    }
}
