using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sim {
    public class Simulation {
        public float deltaTime { get; private set; }
        public float unscaledDeltaTime { get; private set; }
        public long tickCount { get; private set; }

        public List<Unit> units;

        public void Init(int UnitCount = 1) {
            units = new List<Unit>(UnitCount);
            for (int i = 0; i < UnitCount; i++) {
                Unit newU = Unit.Create();
                newU.willDivide = GMath.RandomProb(0.5f);
                newU.Init();
                units.Add(newU);
            }
        }

        public void AddUnit(int unitCount) {
            for (int i = 0; i < unitCount; i++) {
                Unit newU = Unit.Create();
                newU.willDivide = GMath.RandomProb(0.5f);
                newU.Init();
                units.Add(newU);
            }
        }

        public Unit CreateUnit() {
            Unit newU = Unit.Create();
            newU.Init();
            units.Add(newU);
            return newU;
        }

        public void Update(float dt, float udt) {
            tickCount++;
            deltaTime = dt;
            unscaledDeltaTime = udt;
            { // Update Units
                for (int i = 0; i < units.Count; i++) {
                    Unit unit = units[i];
                    unit.Update(this);
                    if (unit.willDispose) {
                        units.RemoveFast(i);
                    }
                }
            }
        }

    } 
}
