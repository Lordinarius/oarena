namespace Sim {
    public class Unit {

        public enum State {
            IDLE,
            WALK,
            ATTACK,
            DEAD
        }

        public UnityEngine.Vector3 position;
        public UnityEngine.Vector3 walkDir;
        public UnityEngine.Vector3 velocity;

        public double health;
        public double armour;
        public UnityEngine.Color color;
        public State state;
        public float timeInState;
        public float walkDirTimer;
        public float walkSpeed = 6;
        public float walkDist;
        public bool willDispose;
        public bool willDivide;

        public void Init() {
            walkDir = GMath.RandomNV2();
            SetState(State.WALK);
        }

        public void Update(Simulation sim) {
            //position += walkDir * 3 * dt;
            float dt = sim.deltaTime;
            timeInState += dt;
            velocity = UnityEngine.Vector3.Lerp(velocity, walkDir, dt * 5);
            UnityEngine.Vector3 d = velocity * walkSpeed * dt;
            if (timeInState >= 3 && GMath.RandomProb(0.5f)) {
                if (state == State.IDLE) {
                    SetState(State.WALK);
                } else if (state == State.WALK) {
                    SetState(State.ATTACK);
                } else if (state == State.ATTACK) {
                    SetState(State.WALK);
                }
            }

            if (state == State.WALK) {
                if (walkDirTimer >= 1) {
                    if (position.magnitude > 25) {
                        walkDir = -position.normalized;
                    } else {
                        walkDir = GMath.RandomNV2();
                    }
                    walkDirTimer = 0;
                }
                walkDirTimer += dt;
                position += d;
                walkDist += d.magnitude;
            } else if (state == State.ATTACK) {
                health -= (10 / armour) * dt;
            } else if (state == State.DEAD) {
                if (timeInState > 1.6f) {
                    willDispose = true;
                    if (willDivide) {
                        sim.CreateUnit().SetPosition(position + new UnityEngine.Vector3(0.5f, 0, 0)).SetVelocity(walkDir * 5).SetWalkDir(-walkDir).willDivide = GMath.RandomProb(0.5f);
                        sim.CreateUnit().SetPosition(position + new UnityEngine.Vector3(-0.5f, 0, 0)).SetVelocity(-walkDir * 5).SetWalkDir(walkDir).willDivide = GMath.RandomProb(0.5f);
                    }
                }
            }
            if (health <= 0 && state != State.DEAD) {
                SetState(State.DEAD);
            }
        }

        private void SetState(State newState) {
            if (state == newState) return;
            state = newState;
            timeInState = 0;
        }

        //Public chain functions
        public Unit SetPosition(UnityEngine.Vector3 pos) {
            position = pos;
            return this;
        }

        public Unit SetWalkDir(UnityEngine.Vector3 dir) {
            walkDir = dir;
            return this;
        }

        public Unit SetVelocity(UnityEngine.Vector3 vel) {
            velocity = vel;
            return this;
        }

        public static Unit Create() {
            return new Unit {
                armour = GMath.RandomRange(5, 10),
                health = GMath.RandomRange(5, 8),
                position = GMath.RandomRV2() * 5,
                color = GMath.RandomColor(0, 1, 0.5f, 1),
                walkSpeed = GMath.RandomRange(3, 7)
            };
        }
    }
}
