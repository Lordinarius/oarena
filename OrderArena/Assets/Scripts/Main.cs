using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour {

    private Sim.Simulation simulation;
    [SerializeField] private Render.RenderManager renderManager;
    [SerializeField] private UI.UIManager uiManager;

    private int simSpeedIndex = 3;
    private float[] simulationSpeeds = new float[] { 0.03f, 0.1f, 0.5f, 1, 2, 5, 10, 25, 50, 100, 1000, 5000, 10000 };

    Com.CommandBucket commands;

    private void Awake () {
        simulation = new Sim.Simulation();
        commands = new Com.CommandBucket();
    }

    private void Start() {
        simulation.Init();
        renderManager.Init();
        uiManager.Init(simulation, commands);
    }

    private void Update () {
        UpdateCheatInput();
        //Invoke commands
        commands.Invoke(simulation);
        //Simulation Update
        {
            float dt = Time.smoothDeltaTime;
            float unscaledDt = dt;
            float simSpeed = simulationSpeeds[simSpeedIndex];
            if (simSpeed < 1) {
                dt *= simSpeed;
            }
            int updateCount = simSpeed > 1 ? Mathf.CeilToInt(simSpeed) : 1;
            if (updateCount > 1) {
                unscaledDt /= updateCount;
            }
            for (int i = 0; i < updateCount; i++) {
                simulation.Update(dt, unscaledDt);
            }
        }
        renderManager.RenderUpdate(simulation);
        uiManager.OnUpdate(simulation);
    }

    private void UpdateCheatInput() {
        if (Input.GetKeyDown(KeyCode.KeypadPlus)) {
            simSpeedIndex++;
            if (simSpeedIndex >= simulationSpeeds.Length) {
                simSpeedIndex = simulationSpeeds.Length - 1;
            }
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus)) {
            simSpeedIndex--;
            if (simSpeedIndex <= 0) {
                simSpeedIndex = 0;
            }
        }
        if (Input.GetKeyDown(KeyCode.KeypadMultiply)) {
            simSpeedIndex = 3;
        }
    }
}
