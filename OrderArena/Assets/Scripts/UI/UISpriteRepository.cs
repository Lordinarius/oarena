using UnityEngine;

namespace UI {
    [CreateAssetMenu(menuName = "Scriptables/Sprite Repository")]
    public class UISpriteRepository : ScriptableObject { }
}
