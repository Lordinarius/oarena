using UnityEngine;

namespace UI {
    [CreateAssetMenu(menuName = "Scriptables/UI Repository")]
    public class UIRepository : ScriptableObject {
        public UISpriteRepository spriteRepository;
        public UIPrefabRepository prefabRepository;
        //Rest
    }
}
