﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace UI {
    [AddComponentMenu("UI/Button", 29)]
    public class UIButton : Selectable, IPointerClickHandler, ISubmitHandler {
        [Serializable]
        public class ButtonEvent : UnityEvent { }

        // Event delegates triggered on click.
        [FormerlySerializedAs("onPress")]
        [SerializeField]
        private ButtonEvent m_onPress;

        public ButtonEvent onPress {
            get { return m_onPress; }
            set { m_onPress = value; }
        }

        [FormerlySerializedAs("onRelease")]
        [SerializeField]
        private ButtonEvent m_onRelease;

        public ButtonEvent onRelease {
            get { return m_onRelease; }
            set { m_onRelease = value; }
        }


        [FormerlySerializedAs("onClick")]
        [SerializeField]
        private ButtonEvent m_onClick = new ButtonEvent();

        protected UIButton() { }

        public ButtonEvent onClick {
            get { return m_onClick; }
            set { m_onClick = value; }
        }

        private void Press() {
            if (!IsActive() || !IsInteractable())
                return;

            UISystemProfilerApi.AddMarker("Button.onClick", this);
            m_onClick.Invoke();
        }

        // Trigger all registered callbacks.
        public virtual void OnPointerClick(PointerEventData eventData) {
            if (eventData.button != PointerEventData.InputButton.Left)
                return;

            Press();
        }

        public override void OnPointerDown(PointerEventData eventData) {
            base.OnPointerDown(eventData);
            m_onPress.Invoke();
        }

        public override void OnPointerUp(PointerEventData eventData) {
            base.OnPointerUp(eventData);
            m_onRelease.Invoke();
        }

        public virtual void OnSubmit(BaseEventData eventData) {
            Press();

            // if we get set disabled during the press
            // don't run the coroutine.
            if (!IsActive() || !IsInteractable())
                return;

            DoStateTransition(SelectionState.Pressed, false);
            StartCoroutine(OnFinishSubmit());
        }

        private IEnumerator OnFinishSubmit() {
            var fadeTime = colors.fadeDuration;
            var elapsedTime = 0f;

            while (elapsedTime < fadeTime) {
                elapsedTime += Time.unscaledDeltaTime;
                yield return null;
            }

            DoStateTransition(currentSelectionState, false);
        }
    } 
}
