using UnityEngine;

namespace UI {
    [CreateAssetMenu(menuName = "Scriptables/Prefab Repository")]
    public class UIPrefabRepository : ScriptableObject { }
}
