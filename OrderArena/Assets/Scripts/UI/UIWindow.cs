namespace UI {
    public abstract class UIWindow : UIWidget {

        public abstract void OnGoBack();

        protected abstract override void OnInit();
        protected abstract override void OnUpdate();

        protected override void OnClose() {
            gameObject.SetActive(false);
        }

        protected override void OnOpen() {
            gameObject.SetActive(true);
        }

    }
}
