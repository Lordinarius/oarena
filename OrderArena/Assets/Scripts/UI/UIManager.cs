using System;
using System.Collections.Generic;
using UnityEngine;
using Com;

namespace UI {
    public class UIManager : MonoBehaviour {

        public UIRepository managerRepository;
        public Sim.Simulation sim;
        public CommandBucket commandBucket;
        
        [Header("Windows")]
        [NonSerialized] public List<UIWindow> windows;
        private UIWindow currentWindow;

        #region Main Control
        public void Init(Sim.Simulation sim,CommandBucket comBucket) {
            this.sim = sim;
            windows = new List<UIWindow>();
            commandBucket = comBucket;
            //Add your windows here

            //Initialize
            for (int i = windows.Count - 1; i >= 0; i--) {
                windows[i].Init(this);
            }

        }        

        private void AddWindow(UIWindow window) {
            windows.Add(window);
        }

        public void OnUpdate(Sim.Simulation sim) {
            this.sim = sim;
            for (int i = 0; i < windows.Count; i++) {
                if (windows[i].gameObject.activeInHierarchy) {
                    windows[i].UpdateObject(this);
                }
            }
        }
        #endregion
        #region Navigation
        public void OpenWindow(UIWindow window) {
            if (currentWindow != null) {
                currentWindow.Close(this);
            }
            window.Open(this);
            currentWindow = window;
        }
        #endregion
    }
}
