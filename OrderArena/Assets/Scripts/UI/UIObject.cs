using UnityEngine;

namespace UI {
    public abstract class UIObject : MonoBehaviour {
        private RectTransform m_rectTransform;
        public RectTransform rectTransform { get { return m_rectTransform ?? (m_rectTransform = GetComponent<RectTransform>()); } }
    }
}
