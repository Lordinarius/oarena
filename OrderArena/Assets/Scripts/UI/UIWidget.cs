namespace UI {
    public abstract class UIWidget : UIObject {

        public UIWidget parent;
        public UIManager manager;

        public void Open(UIManager manager, UIWidget parent = null) {
            this.manager = manager;
            this.parent = parent;
            OnOpen();
        }

        public void Close(UIManager manager) {
            this.manager = manager;
            OnClose();
        }


        public void Init(UIManager manager) {
            this.manager = manager;
            OnInit();
        }

        public void UpdateObject(UIManager manager) {
            this.manager = manager;
            OnUpdate();
        }

        protected abstract void OnInit();
        protected abstract void OnUpdate();
        protected abstract void OnOpen();
        protected abstract void OnClose();
    }
}
