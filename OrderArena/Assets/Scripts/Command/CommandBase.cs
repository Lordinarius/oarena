﻿using Sim;
namespace Com {
    public abstract class CommandBase {
        public int priority = -1;
        public abstract void Invoke(Simulation sim);
    } 
}
