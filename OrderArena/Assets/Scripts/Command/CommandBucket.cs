﻿using System.Collections.Generic;
namespace Com {
    public class CommandBucket {
        public List<CommandBase> commands;
        public CommandBucket() {
            commands = new List<CommandBase>();
        }
        
        public void Invoke(Sim.Simulation sim) {
            int comCount = commands.Count;
            for (int i = 0; i < comCount; i++) {
                commands[i].Invoke(sim);
            }
            commands.Clear();
        }

        public void AddCommand(CommandBase command) {
            if (command.priority == -1) {
                commands.Add(command);
            } else {
                for (int i = commands.Count - 1; i >= 0; i--) {
                    if (command.priority < commands[i].priority) {
                        commands.Insert(i, command);
                        return;
                    }
                }
                commands.Add(command);
            }
        }
    } 
}
