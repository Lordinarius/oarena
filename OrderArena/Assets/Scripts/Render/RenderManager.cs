using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sim;

namespace Render {
    public class RenderManager : MonoBehaviour {

        public Simulation sim;

        public UnitRenderer heroRendererPrefab;
        private RendererBucket<Unit> heroRendererBucket;
        private event System.Action collector;
        private event System.Action resetter;

        public void Init() {
            heroRendererBucket = CreateBucket(heroRendererPrefab,10);
        }

        //Main render loop
        public void RenderUpdate(Simulation sim) {
            float dt = sim.deltaTime;
            bool simReset = false;
            if (this.sim != sim) {
                this.sim = sim;
                simReset = true;
            }
            //Reset all buckets if there is a new simulation
            if(simReset) {
                resetter.Invoke();
            }
            //Update renderers: Add your updates (you can use one bucket for multiple times);
            {
                heroRendererBucket.UpdateRenderer(dt, sim.units, this);
            }
            //Collect and despawn unused renderers
            {
                collector.Invoke();
            }
        }

        private RendererBucket<T> CreateBucket<T>(RenderObject<T> prefab, int poolPassiveCap) {
            var newBucket = new RendererBucket<T>(prefab, 10);
            collector += newBucket.CollectUnused;
            resetter += newBucket.Reset;
            return newBucket;
        }

        private void DestroyBucket<T>(RendererBucket<T> bucket) {
            bucket.DestroyAll();
            collector -= bucket.CollectUnused;
            resetter -= bucket.Reset;
        }   

        private void UpdateRenderer<T>(float dt, List<T> objects, RendererBucket<T> bucket) {
            for (int i = 0; i < objects.Count; i++) {
                T unit = objects[i];
                RenderObject<T> renderer = null;
                if (!bucket.rendererPairs.TryGetValue(unit, out renderer)) {
                    renderer = bucket.CreatePair(unit, this);
                    if (!renderer.isInitialized) {
                        renderer.isInitialized = true;
                        renderer.Init(this, unit);
                    }
                    renderer.Reset(this, unit);
                }
                renderer.UpdateRender(this, unit, dt);
                renderer.willDispose = false;
            }
        }

        private void CollectUnused<T>(RendererBucket<T> bucket) {
            for (int i = 0; i < bucket.renderers.Count; i++) {
                RenderObject<T> r = bucket.renderers[i];
                if (r.willDispose) {
                    bucket.RemovePair(i);
                } else {
                    r.willDispose = true;
                }
            }
        }
    }

    public class RendererBucket<T> {
        public Pooler<RenderObject<T>> rendererPool;
        public List<RenderObject<T>> renderers;
        public List<T> simobjects;
        public Dictionary<T, RenderObject<T>> rendererPairs;

        public void DestroyAll() {
            rendererPool.DestroyAll();
            renderers.Clear();
            simobjects.Clear();
            rendererPairs.Clear();
        }

        public void Reset() {
            rendererPool.DespawnAll();
            renderers.Clear();
            simobjects.Clear();
            rendererPairs.Clear();
        }

        public RenderObject<T> CreatePair(T simObject, RenderManager renderManager) {
            RenderObject<T> newRenderer = rendererPool.Spawn();
            rendererPairs.Add(simObject, newRenderer);
            simobjects.Add(simObject);
            renderers.Add(newRenderer);
            return newRenderer;
        }

        public void RemovePair(T simObject) {
            RenderObject<T> renderer = rendererPairs[simObject];
            rendererPool.Despawn(renderer);
            rendererPairs.Remove(simObject);
            simobjects.RemoveFast(simObject);
            renderers.RemoveFast(renderer);
        }

        public void RemovePair(int index) {
            T so = simobjects[index];
            RenderObject<T> renderer = rendererPairs[so];
            rendererPool.Despawn(renderer);
            rendererPairs.Remove(so);
            simobjects.RemoveFast(index);
            renderers.RemoveFast(index);
        }

        public RendererBucket(RenderObject<T> prefab, int poolPassiveCap) {
            rendererPool = new Pooler<RenderObject<T>>(prefab, poolPassiveCap);
            rendererPairs = new Dictionary<T, RenderObject<T>>();
            renderers = new List<RenderObject<T>>();
            simobjects = new List<T>();
        }

        public RendererBucket(Pooler<RenderObject<T>> pooler) {
            rendererPool = pooler;
            rendererPairs = new Dictionary<T, RenderObject<T>>();
            renderers = new List<RenderObject<T>>();
            simobjects = new List<T>();
        }

        public void UpdateRenderer(float dt, List<T> objects, RenderManager renderManager) {
            for (int i = 0; i < objects.Count; i++) {
                T unit = objects[i];
                RenderObject<T> renderer = null;
                if (!rendererPairs.TryGetValue(unit, out renderer)) {
                    renderer = CreatePair(unit, renderManager);
                    if (!renderer.isInitialized) {
                        renderer.isInitialized = true;
                        renderer.Init(renderManager, unit);
                    }
                    renderer.Reset(renderManager, unit);
                }
                renderer.UpdateRender(renderManager, unit, dt);
                renderer.willDispose = false;
            }
        }

        public void UpdateRenderer(float dt, T obj, RenderManager renderManager) {
            RenderObject<T> renderer = null;
            if (!rendererPairs.TryGetValue(obj, out renderer)) {
                renderer = CreatePair(obj, renderManager);
                if (!renderer.isInitialized) {
                    renderer.isInitialized = true;
                    renderer.Init(renderManager, obj);
                }
                renderer.Reset(renderManager, obj);
            }
            renderer.UpdateRender(renderManager, obj, dt);
            renderer.willDispose = false;
        }

        public void CollectUnused() {
            for (int i = 0; i < renderers.Count; i++) {
                RenderObject<T> r = renderers[i];
                if (r.willDispose) {
                    RemovePair(i);
                } else {
                    r.willDispose = true;
                }
            }
        }
    }

    public abstract class RenderObject<T> : MonoBehaviour {
        public T simObject;
        public bool willDispose = true;
        public bool isInitialized = false;
        public abstract void Init(RenderManager renderManager, T unit);
        public abstract void Reset(RenderManager renderManager, T unit);
        public abstract void UpdateRender(RenderManager renderManager, T unit, float dt);
    }
}
