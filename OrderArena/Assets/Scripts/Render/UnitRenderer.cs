using UnityEngine;
using System.Collections;
using Sim;

namespace Render {
    public class UnitRenderer : RenderObject<Sim.Unit> {

        public SpriteRenderer spriteRenderer;
        private float renderScale = 5;
        //public Sim.Unit simObject;

        public override void Init(RenderManager renderManager, Unit unit) {
            //throw new System.NotImplementedException();
        }

        public override void Reset(RenderManager renderManager, Unit unit) {
            //throw new System.NotImplementedException();
            spriteRenderer.color = Color.white;
        }

        public override void UpdateRender(RenderManager renderManager, Unit unit, float dt) {
            simObject = unit;
            transform.position = simObject.position;
            float te = (float)(simObject.health % 2);
            if (te > 1) {
                te = 2 - te;
            }
            
            if (unit.willDivide) {
                var c = spriteRenderer.color;
                c.r = 0.6f;
                spriteRenderer.color = c;
            }
            Vector3 targetScale;
            switch (unit.state) {
                case Unit.State.IDLE:
                    transform.localScale = new Vector3(1, 1 + 0.1f * Mathf.PingPong(unit.timeInState, 1), 0.2f) * renderScale;
                    break;
                case Unit.State.WALK:
                    targetScale = new Vector3(1, 1 + 0.5f * Mathf.PingPong(unit.timeInState * 5, 1), 0.2f) * renderScale;
                    transform.localScale = Vector3.Lerp(transform.localScale, targetScale,dt*10);
                    break;
                case Unit.State.ATTACK:
                    targetScale = new Vector3(1, 1, 1) * renderScale + Vector3.one * 0.5f * Mathf.PingPong(unit.timeInState, 0.3f);
                    transform.localScale = Vector3.Lerp(transform.localScale, targetScale, dt*10);
                    break;
                case Unit.State.DEAD:
                    float t = Mathf.Lerp(0, 1, unit.timeInState / 1.6f);
                    if (unit.willDivide) {
                        Vector3 r = GMath.RandomNV2();
                        transform.position = transform.position + r * 0.05f;
                        transform.localScale = new Vector3(1 + t * 0.8f, 1 + t / 6, 1)*renderScale;
                        var c = spriteRenderer.color;
                        c.g = 1 - t;
                        c.b = 1 - t;
                        spriteRenderer.color = c;
                    } else {
                        var c = spriteRenderer.color;
                        c.a = Mathf.Lerp(1, 0, t * 2);
                        spriteRenderer.color = c;
                        transform.localScale = Vector3.one * renderScale + new Vector3(1 - t * 0.8f, 1 - t * 0.8f, 1);
                    }
                    break;
                default: throw new System.Exception(unit.state.ToString());
            }
            spriteRenderer.sortingOrder = (int)(5000 - unit.position.y * 100);
        }
    } 
}
