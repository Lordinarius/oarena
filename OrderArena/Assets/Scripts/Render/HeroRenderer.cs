using System.Collections;
using System.Collections.Generic;
using Sim;
using UnityEngine;

namespace Render {
    public class HeroRenderer : RenderObject<Unit> {

        public Spine.Unity.SkeletonRenderer skeletonAnimation;
        //Spine.AnimationState spineAnimationState;
        Spine.Skeleton skeleton;

        [SerializeField, Spine.Unity.SpineAnimation]
        private string walkAnimation;
        private Spine.Animation walkAnimationData;
        [SerializeField, Spine.Unity.SpineAnimation]
        private string attackAnimation;
        private Spine.Animation attackAnimationData;
        [SerializeField, Spine.Unity.SpineAnimation]
        private string deadAnimation;
        private Spine.Animation deadAnimationData;

        Unit.State lastState;

        private float animAlpha;

        private const float VAL_ALPHA_SPEED = 1f;

        public override void Init(RenderManager renderManager, Unit unit) {
            skeleton = skeletonAnimation.skeleton;
            walkAnimationData = skeleton.Data.FindAnimation(walkAnimation);
            attackAnimationData = skeleton.Data.FindAnimation(attackAnimation);
            deadAnimationData = skeleton.Data.FindAnimation(deadAnimation);
        }

        public override void Reset(RenderManager renderManager, Unit unit) {
            skeleton.R = 1;
            skeleton.G = 1;
            skeleton.B = 1;
            skeleton.A = 1;
            transform.localScale = new Vector3(1, 1, 1);
        }

        public override void UpdateRender(RenderManager renderManager, Unit unit, float dt) {
            if (lastState != unit.state) {
                animAlpha = 0;
            }
            transform.position = unit.position;
            Spine.Animation animation = null;
            float animSpeed = 1;
            if (unit.willDivide) {
                skeleton.R = 0.6f;
            }
            switch (unit.state) {
                case Unit.State.IDLE:
                    break;
                case Unit.State.WALK:
                    animation = walkAnimationData;
                    skeleton.UpdateAnim(animation, 1, unit.walkDist * 0.4f, true);
                    break;
                case Unit.State.ATTACK:
                    animation = attackAnimationData;
                    skeleton.UpdateAnim(animation, animAlpha, unit.timeInState * animSpeed, true);
                    break;
                case Unit.State.DEAD:
                    animation = deadAnimationData;
                    animSpeed = 2;
                    skeleton.UpdateAnim(animation, animAlpha, unit.timeInState * animSpeed, false);
                    float t = Mathf.Lerp(0, 1, unit.timeInState / 1.6f);
                    if (unit.willDivide) {
                        Vector3 r = GMath.RandomNV2();
                        transform.position = transform.position + r * 0.05f;
                        transform.localScale = new Vector3(1 + t * 0.8f, 1 + t / 6, 1);
                        skeleton.G = 1 - t;
                        skeleton.B = 1 - t;
                    } else {
                        skeleton.A = Mathf.Lerp(1, 0, t*2);
                        transform.localScale = new Vector3(1 - t * 0.8f, 1 - t * 0.8f, 1);
                    }
                    break;
                default: throw new System.Exception(unit.state.ToString());
            }
            animAlpha += dt * VAL_ALPHA_SPEED;
            if (animAlpha > 1) {
                animAlpha = 1;
            }
            skeleton.FlipX = unit.walkDir.x < 0;
            skeletonAnimation.meshRenderer.sortingOrder = (int)(5000 - unit.position.y * 100);
            lastState = unit.state;
        }

        
    }
}