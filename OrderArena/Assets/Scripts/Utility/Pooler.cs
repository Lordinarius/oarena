using System;
using System.Collections;
using System.Collections.Generic;
using UObject = UnityEngine.Object;
using MBehaviour = UnityEngine.MonoBehaviour;

public class Pooler<T> where T : MBehaviour {
    private T original;
    private Queue<T> passiveStack;
    private HashSet<T> activeStack;

    private int passiveCounter;
    private int lastCount;
    private int passiveCap;

    public Pooler(T original) {
        this.original = original;
        passiveCap = 10;
        passiveStack = new Queue<T>();
        activeStack = new HashSet<T>();
    }

    public Pooler(T original,int passiveCap) {
        this.original = original;
        this.passiveCap = passiveCap;
        passiveStack = new Queue<T>();
        activeStack = new HashSet<T>();
    }

    public T Spawn() {
        if (passiveStack.Count > 0) {
            if (passiveStack.Count > passiveCap) {
                passiveCounter++;
            }
            T toSpawn = passiveStack.Dequeue();
            activeStack.Add(toSpawn);
            toSpawn.gameObject.SetActive(true);
            if (passiveCounter > 20) {
                CollectUnused();
            }
            return toSpawn;
        } else {
            T toSpawn = UObject.Instantiate(original);
            activeStack.Add(toSpawn);
            return toSpawn;
        }


    }

    private void CollectUnused() {
        int toRemove = passiveStack.Count / 2;
        for (int i = 0; i < toRemove; i++) {
            UObject.Destroy(passiveStack.Dequeue().gameObject);
        }
        passiveCounter = 0;
    }

    public void Despawn(MBehaviour obj) {
        T o = (T)obj;
        if (activeStack.Contains(o)) {
            //o.OnDespawn();
            o.gameObject.SetActive(false);
            activeStack.Remove(o);
            passiveStack.Enqueue(o);
        } else {
            throw new System.Exception("That item is not on the stack");
        }
    }

    public void DespawnAll() {
        foreach (var o in activeStack) {
            o.gameObject.SetActive(false);
            passiveStack.Enqueue(o);
        }
        activeStack.Clear();
    }

    public void DestroyAll() {
        foreach (var o in activeStack) {
            UObject.Destroy(o.gameObject);
        }
        foreach (var o in passiveStack) {
            UObject.Destroy(o.gameObject);
        }
        activeStack.Clear();
        passiveStack.Clear();
    }

    //public static implicit operator Pooler<T>(Pooler<Render.HeroRenderer> v) {
    //    return v;
    //}
}