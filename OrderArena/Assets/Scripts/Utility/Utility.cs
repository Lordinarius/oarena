using UnityEngine;
using System.Collections.Generic;

public static class Utility {
    public static void RemoveFast<T>(this List<T> list, int index) {
        list[index] = list[list.Count - 1];
        list.RemoveAt(list.Count - 1);
    }
    public static void RemoveFast<T>(this List<T> list, T item) {
        int index = list.IndexOf(item);
        list[index] = list[list.Count - 1];
        list.RemoveAt(list.Count - 1);
    }
    public static void UpdateAnim(this Spine.Skeleton skeleton, Spine.Animation animation, float alpha, float time, bool loop) {
        animation.Apply(skeleton, 0, time, loop, null, alpha, Spine.MixPose.Current, Spine.MixDirection.In);
        skeleton.UpdateWorldTransform();
    }    

    public static void PosX(this Transform transform, float pos) {
        transform.position = new Vector3(pos, transform.position.y, transform.position.z);
    }

    public static void PosY(this Transform transform, float pos) {
        transform.position = new Vector3(transform.position.x, pos, transform.position.z);
    }

    public static void PosZ(this Transform transform, float pos) {
        transform.position = new Vector3(transform.position.x, transform.position.y, pos);
    }

    public static void ScaleOne(this Transform transform) {
        transform.localScale = Vector3.one;
    }

    public static void ScaleZero(this Transform transform) {
        transform.localScale = Vector3.zero;
    }

    public static void AncPosX(this RectTransform rectTransform, float pos) {
        rectTransform.anchoredPosition3D = new Vector3(pos, rectTransform.position.y, rectTransform.position.z);
    }

    public static void AncPosY(this RectTransform rectTransform, float pos) {
        rectTransform.anchoredPosition3D = new Vector3(rectTransform.position.x, pos, rectTransform.position.z);
    }

    public static void AncPosZ(this RectTransform rectTransform, float pos) {
        rectTransform.anchoredPosition3D = new Vector3(rectTransform.position.x, rectTransform.position.y, pos);
    }
    
    public enum Anchor {
        CENTER,
        TOP,
        RIGHT,
        LEFT,
        BOTTOM,
        TOPRIGHT,
        TOPLEFT,
        BOTTOMRIGHT,
        BOTTOMLEFT,
    }

    public static void SetAnchor(this RectTransform rectTransform, Anchor anchor, bool self = false) {
        switch (anchor) {
            case Anchor.CENTER:
                rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
                rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
                if (self) { rectTransform.pivot = new Vector2(0.5f, 0.5f); }
                break;
            case Anchor.TOP:
                rectTransform.anchorMax = new Vector2(0.5f, 1f);
                rectTransform.anchorMin = new Vector2(0.5f, 1f);
                if (self) { rectTransform.pivot = new Vector2(0.5f, 1f); }
                break;
            case Anchor.RIGHT:
                rectTransform.anchorMax = new Vector2(1f, 0.5f);
                rectTransform.anchorMin = new Vector2(1f, 0.5f);
                if (self) { rectTransform.pivot = new Vector2(1f, 0.5f); }
                break;
            case Anchor.LEFT:
                rectTransform.anchorMax = new Vector2(0, 0.5f);
                rectTransform.anchorMin = new Vector2(0, 0.5f);
                if (self) { rectTransform.pivot = new Vector2(0, 0.5f); }
                break;
            case Anchor.BOTTOM:
                rectTransform.anchorMax = new Vector2(0.5f, 0);
                rectTransform.anchorMin = new Vector2(0.5f, 0);
                if (self) { rectTransform.pivot = new Vector2(0.5f, 0); }
                break;
            case Anchor.TOPRIGHT:
                rectTransform.anchorMax = new Vector2(1, 1);
                rectTransform.anchorMin = new Vector2(1, 1);
                if (self) { rectTransform.pivot = new Vector2(1, 1); }
                break;
            case Anchor.TOPLEFT:
                rectTransform.anchorMax = new Vector2(0, 1);
                rectTransform.anchorMin = new Vector2(0, 1);
                if (self) { rectTransform.pivot = new Vector2(0, 1); }
                break;
            case Anchor.BOTTOMRIGHT:
                rectTransform.anchorMax = new Vector2(1, 0);
                rectTransform.anchorMin = new Vector2(1, 0);
                if (self) { rectTransform.pivot = new Vector2(1, 0); }
                break;
            case Anchor.BOTTOMLEFT:
                rectTransform.anchorMax = new Vector2(0, 0);
                rectTransform.anchorMin = new Vector2(0, 0);
                if (self) { rectTransform.pivot = new Vector2(0, 0); }
                break;

            default: throw new System.Exception(anchor.ToString());
        }
    }
}

public static class GMath {
    public static float RandomRange(float min, float max) {
        return Random.Range(min, max);
    }

    public static bool RandomProb(float prob) {
        return Random.Range(0f, 1f) <= prob;
    }

    public static float Cos(float ang) {
        return Mathf.Cos(ang);
    }

    public static float Sin(float ang) {
        return Mathf.Sin(ang);
    }

    public static Color RandomColor(float hueMin, float hueMax, float saturationMin, float saturationMax) {
        return Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax, 0.3f, 1);
    }
    public static Vector2 RandomV2() {
        return Random.insideUnitCircle;
    }
    public static Vector2 RandomRV2() {
        return new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
    }
    public static Vector2 RandomNV2() {
        return Random.onUnitSphere;
    }
}